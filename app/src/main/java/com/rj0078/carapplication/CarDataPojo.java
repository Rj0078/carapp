package com.rj0078.carapplication;

/**
 * Created by Hrishi0078 on 9/1/2016 at 6:59 PM.
 */
public class CarDataPojo {
    String make, model,number;
    int carOwnerId;

    public CarDataPojo(String make, String model, String number, int carOwnerId) {
        this.make = make;
        this.model = model;
        this.number = number;
        this.carOwnerId = carOwnerId;
    }

    public int getCarOwnerId() {
        return carOwnerId;
    }

    public void setCarOwnerId(int carOwnerId) {
        this.carOwnerId = carOwnerId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
