package com.rj0078.carapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CarDetailsActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    Button btn_car_amt;
    EditText car_amt;
    TextView submit;
    int car_total;
    Dbhelper dbhelper;
    int userId;
 List<CarDataPojo> carDataPojos =new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_car_data);
        linearLayout= (LinearLayout) findViewById(R.id.root_linear_layout);
        submit= (TextView) findViewById(R.id.submit_car_dtls);
        car_amt= (EditText) findViewById(R.id.car_amount);
        btn_car_amt= (Button) findViewById(R.id.btn_car_amt);
        dbhelper=new Dbhelper(this);
        userId= dbhelper.getUserId(getIntent().getStringExtra("userName"));



        btn_car_amt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.removeAllViews();
                if (car_amt.getText().toString().length() != 0) {
                    car_total = Integer.parseInt(car_amt.getText().toString());
                    for (int i = 0; i < car_total; i++) {
                        View v = getViewtoAdd();
                        linearLayout.addView(v);
                    }
                }
                else
                {
                    Toast.makeText(CarDetailsActivity.this,"Enter Amount Of car!!!",Toast.LENGTH_SHORT).show();
                }
            }

        });
         submit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view)
             {   int flag=0;
                 for(int i=0;i<car_total;i++)
                 {
                 LinearLayout l= (LinearLayout) linearLayout.getChildAt(i);

                 EditText make= (EditText) l.getChildAt(0);
                 EditText model= (EditText) l.getChildAt(1);
                 EditText num= (EditText) l.getChildAt(2);
                     if(make.getText().toString().length()==0 || (model.getText().toString().length()==0||num.getText().length()==0) )
                     {
                         Toast.makeText(CarDetailsActivity.this,"Fields Can't be Blank!!!",Toast.LENGTH_SHORT).show();
                         flag=78;
                         carDataPojos.clear();
                         break;
                     }
                     if ((flag != 78))
                     {
                     carDataPojos.add(new CarDataPojo(make.getText().toString(),model.getText().toString(),num.getText().toString(),userId));
                     Log.d("Rj",make.getText().toString()+model.getText().toString()+num.getText().toString());

                     }
                 }
                 if (carDataPojos.size()!= 0)
                 {
                     dbhelper.addCar(carDataPojos);
                     finish();
                     startActivity(new Intent(CarDetailsActivity.this,MainActivity.class));

                 }

             }
         });





    }
    View getViewtoAdd()
    {
        View view1= getLayoutInflater().inflate(R.layout.car_details, null);
        return view1;

    }
}
