package com.rj0078.carapplication;

/**
 * Created by Hrishi0078 on 9/2/2016 at 3:20 PM.
 */
public class CardDataPojo {
    String username,cardetails;

    public CardDataPojo(String username, String cardetails) {
        this.username = username;
        this.cardetails = cardetails;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCardetails() {
        return cardetails;
    }

    public void setCardetails(String cardetails) {
        this.cardetails = cardetails;
    }
}
