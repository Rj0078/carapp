package com.rj0078.carapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DataDisplayActivity extends AppCompatActivity {
  RecyclerView recyclerView;
    List<CardDataPojo> cardDataPojos=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_display);
        recyclerView= (RecyclerView) findViewById(R.id.recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        cardDataPojos=new Dbhelper(this).getCarData();
        recyclerView.setAdapter(new RecAdapter(this,cardDataPojos));

    }
}
