package com.rj0078.carapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hrishi0078 on 9/2/2016 at 4:05 PM.
 */
public class Dbhelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Car.db";

    public static final String USER_TABLE_NAME = "users";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_NAME = "name";

    public static final String CAR_TABLE_NAME = "car";
    public static final String CAR_COLUMN_ID = "id";

    public static final String CAR_USER_ID = "num";
    public static final String CAR_COLUMN_MODEL = "model";
    public static final String CAR_COLUMN_MAKE = "make";
    public static final String CAR_COLUMN_NUMBER = "car_num";

    public static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS "+ USER_TABLE_NAME +"" + "("+USER_COLUMN_ID +" integer primary key AUTOINCREMENT , "+USER_COLUMN_NAME+" text UNIQUE )";
    public static final String CREATE_CAR_TABLE = "CREATE TABLE IF NOT EXISTS "+ CAR_TABLE_NAME +"" + "("+CAR_COLUMN_ID+" integer primary key, "+CAR_USER_ID+" integer   REFERENCES "+USER_TABLE_NAME+","+ CAR_COLUMN_MAKE+" text not null,"+CAR_COLUMN_MODEL+" text not null,"+CAR_COLUMN_NUMBER+" text not null, FOREIGN KEY("+CAR_USER_ID+") REFERENCES "+USER_TABLE_NAME+"("+USER_COLUMN_ID+"))";

    public Dbhelper(Context context) {
        super(context, /*Environment.getExternalStorageDirectory()
                + File.separator + "RJ"
                + File.separator +*/DATABASE_NAME, null, 3);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
                sqLiteDatabase.execSQL(CREATE_USER_TABLE);
              sqLiteDatabase.execSQL(CREATE_CAR_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long adduser(String username)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COLUMN_NAME, username);

        long rh = db.insert(USER_TABLE_NAME, null, contentValues);
        return rh;


    }
    public long addCar(List<CarDataPojo> carDataPojos)
    {  long rh = 0;
            for (int i = 0; i< carDataPojos.size(); i++)
            {
                SQLiteDatabase db = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(CAR_USER_ID, carDataPojos.get(i).getCarOwnerId());
                contentValues.put(CAR_COLUMN_MAKE, carDataPojos.get(i).getMake());
                contentValues.put(CAR_COLUMN_MODEL, carDataPojos.get(i).getModel());
                contentValues.put(CAR_COLUMN_NUMBER, carDataPojos.get(i).getNumber());

                rh = db.insert(CAR_TABLE_NAME, null, contentValues);

            }
        return rh;


    }
    public  int getUserId(String userName)
    {
        int userId = -2;
        SQLiteDatabase db = this.getWritableDatabase();
          Cursor c = db.rawQuery("SELECT * FROM "+USER_TABLE_NAME+" WHERE "+USER_COLUMN_NAME+" = '"+userName+"'", null);
        if(c.moveToFirst()){
            do{

                 userId = c.getInt(0);
                Log.d("Rj_user_id", String.valueOf(userId));


            }while(c.moveToNext());
        }
    return userId;


    }
    public  String getUserName(String userID)
    {
        String userName = null;
        int userId = -2;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT "+USER_COLUMN_NAME+" FROM "+USER_TABLE_NAME+" WHERE "+USER_COLUMN_ID+" = '"+userID+"'", null);
        if(c.moveToFirst()){
            do{

                 userName = c.getString(0);
                Log.d("Rj_user_id", String.valueOf(userId));


            }while(c.moveToNext());
        }
        return userName;


    }
    public List<CardDataPojo> getCarData()
    {
       List<UserDataPojo> userDataPojoList = this.getUsers();
        List<CardDataPojo> cardDataPojos=new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i=0;i<userDataPojoList.size();i++)
        {   String tempuserid=userDataPojoList.get(i).getUserId();
            String carDetails = "";
            Cursor c = db.rawQuery("SELECT * FROM "+CAR_TABLE_NAME+" WHERE "+CAR_USER_ID+" = '"+tempuserid+"'", null);
            if(c.moveToFirst())
            {

                do{

                    carDetails +=" MAK : "+ c.getString(2)+"\n ";
                    carDetails += "MOD : "+c.getString(3)+"\n ";
                    carDetails += "NUM : "+c.getString(4)+" "+"\n\n";




                }while(c.moveToNext());
                Log.d("Rj_user_id",tempuserid+ String.valueOf(carDetails));
            }
              tempuserid= String.valueOf(this.getUserName(tempuserid));
            cardDataPojos.add(new CardDataPojo(tempuserid,String.valueOf(carDetails)));

        }
        return cardDataPojos;

    }
    public List<UserDataPojo> getUsers()
    {   UserDataPojo userDataPojo;
        List<UserDataPojo> userDataPojos =new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM "+USER_TABLE_NAME, null);
        if(c.moveToFirst()){
            do{

               int userId = c.getInt(0);
                int userName=c.getInt(1);
                userDataPojo=new UserDataPojo(String.valueOf(userId),String.valueOf(userName));
                userDataPojos.add(userDataPojo);
                Log.d("Rj_user_id_name", String.valueOf(userId)+String.valueOf(userName));


            }while(c.moveToNext());
        }
        return userDataPojos;


    }
}
