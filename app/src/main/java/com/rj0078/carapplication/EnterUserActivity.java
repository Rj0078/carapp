package com.rj0078.carapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EnterUserActivity extends AppCompatActivity {
    ImageView imageView;
    EditText editText;
    TextView proceed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        editText= (EditText) findViewById(R.id.username);
        imageView= (ImageView) findViewById(R.id.nextBtn);
        proceed= (TextView) findViewById(R.id.proceed);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (editText.getText().toString().length() != 0)
                {
                    Dbhelper dbhelper = new Dbhelper(EnterUserActivity.this);
                    dbhelper.getWritableDatabase();
                    long result = dbhelper.adduser(editText.getText().toString());
                    if (result == -1) {
                        Toast.makeText(EnterUserActivity.this, "User Already Exist", Toast.LENGTH_LONG).show();
                        proceed.setVisibility(View.VISIBLE);
                    } else {
                        goToCarDetails();
                    }

                }
                else
                {
                    Toast.makeText(EnterUserActivity.this,"Name Can't be Blank!!!",Toast.LENGTH_SHORT).show();
                }

            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCarDetails();
            }
        });
    }

    void goToCarDetails()
    {   finish();
        startActivity(new Intent(EnterUserActivity.this,CarDetailsActivity.class).putExtra("userName",editText.getText().toString()));
    }
}
