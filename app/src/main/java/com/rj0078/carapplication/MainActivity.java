package com.rj0078.carapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
ImageButton add,view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add= (ImageButton) findViewById(R.id.viewdata);
        view= (ImageButton) findViewById(R.id.adddata);
        Dbhelper dbhelper=new Dbhelper(this);
        dbhelper.getWritableDatabase();
        view.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        startActivity(new Intent(MainActivity.this,EnterUserActivity.class));
    }
});

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,DataDisplayActivity.class));
            }
        });
    }
}
