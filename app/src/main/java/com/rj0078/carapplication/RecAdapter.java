package com.rj0078.carapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hrishi0078 on 9/2/2016 at 3:15 PM.
 */
public class RecAdapter extends RecyclerView.Adapter<RecAdapter.CarViewHolder> {
Context context;
    List<CardDataPojo> cardDataPojos;

    public RecAdapter(Context context, List<CardDataPojo> cardDataPojoList)
    {
        cardDataPojos=cardDataPojoList;
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.car_details_card,parent,false);
        CarViewHolder carViewHolder=new CarViewHolder(view);
        return carViewHolder;
    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, int position) {
        final CardDataPojo pojoData=this.cardDataPojos.get(position);

            holder.username.setText(cardDataPojos.get(position).getUsername());
            holder.cardetails.setText(cardDataPojos.get(position).getCardetails());

    }

    @Override
    public int getItemCount() {
        return cardDataPojos.size();
    }

    static class CarViewHolder extends RecyclerView.ViewHolder{
      TextView username,cardetails;
        public CarViewHolder(View itemView) {
            super(itemView);
            username= (TextView) itemView.findViewById(R.id.user_name);
            cardetails= (TextView) itemView.findViewById(R.id.car_details);
        }
    }
}
