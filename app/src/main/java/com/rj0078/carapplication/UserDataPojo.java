package com.rj0078.carapplication;

/**
 * Created by Hrishi0078 on 9/2/2016 at 4:00 PM.
 */
public class UserDataPojo {
    String userId,userName;

    public UserDataPojo(String userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
